**Update:** the proposed re-structuring of the official Nvidia Docker images [has been integrated](https://gitlab.com/nvidia/cuda/issues/3#note_41972129).
Please see the new `base`, `runtime`, `devel` flavors of the [official images](https://gitlab.com/nvidia/cuda).

in CUDA 9.0:
- `base`: 123 MB
- `runtime`: +571 MB
- `devel`: +1031 MB

The repo here, as a proof of concept, will continue to stay unmaintained.

# Ubuntu 16.04 [![build status](https://gitlab.com/ax3l/docker-cuda/badges/ubuntu16.04-minimal/build.svg)](https://gitlab.com/ax3l/docker-cuda/commits/ubuntu16.04-minimal)

This is an inofficial fork from Nvidia's default Docker images to provide smaller layers with a minimal CUDA install.

Thanks for the great setup and for sharing the Dockerfile!
The only idea of this repo is to keep the install smaller when pulling layers for depending projects.
Since the deb (apt) repo is installed, too you can always add additional dependencies in your projects if *this* repo is *too narrow* for you.
Please refer to the originial `Dockerfile` for package names.

## CUDA 8.0
- [`8.0-runtime-minimal`, `8.0-runtime-ubuntu16.04-minimal` (*8.0/runtime/Dockerfile*)](https://gitlab.com/ax3l/docker-cuda/blob/ubuntu16.04-minimal/8.0/runtime/Dockerfile)
- [`8.0-devel-minimal`, `latest`, `8.0-devel-ubuntu16.04-minimal` (*8.0/devel/Dockerfile*)](https://gitlab.com/ax3l/docker-cuda/blob/ubuntu16.04-minimal/8.0/devel/Dockerfile)

## Provided Features

### Runtime
- cuda RT
- setup of deb repo (if you need more installed such as cuBLAS, cuFFT, etc.)

### Devel
- development headers for
  - cuda core
  - misc headers
  - driver
  - RT
- cmd line tools
- cuda NVML (+headers)

## Usage

In your Dockerfile:
```
FROM registry.gitlab.com/ax3l/docker-cuda:8.0-devel-minimal
```

or on command line:
```
docker pull registry.gitlab.com/ax3l/docker-cuda:8.0-runtime-minimal
# 13 layers
# 119.2 MB

docker pull nvidia/cuda:8.0-runtime
# 18 layers
# 778.2 MB
```

and for the reduced development environment:
```
docker pull registry.gitlab.com/ax3l/docker-cuda:8.0-devel-minimal
# 16 layers
# 403.9 MB

docker pull nvidia/cuda:8.0-devel
# 21 layers
# 1.671 GB
```
